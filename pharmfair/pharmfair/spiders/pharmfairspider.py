import json
from scrapy import Spider, FormRequest

formdata = {"draw": "11",
            "columns[0][data]": "0",
            "columns[0][name]": "license",
            "columns[0][searchable]": "true",
            "columns[0][orderable]": "true",
            "columns[0][search][value]": "",
            "columns[0][search][regex]": "false",
            "columns[1][data]": "1",
            "columns[1][name]": "brand",
            "columns[1][searchable]": "true",
            "columns[1][orderable]": "true",
            "columns[1][search][value]": "",
            "columns[1][search][regex]": "false",
            "columns[2][data]": "2",
            "columns[2][name]": "ingredients",
            "columns[2][searchable]": "true",
            "columns[2][orderable]": "true",
            "columns[2][search][value]": "",
            "columns[2][search][regex]": "false",
            "columns[3][data]": "3",
            "columns[3][name]": "registrant",
            "columns[3][searchable]": "true",
            "columns[3][orderable]": "true",
            "columns[3][search][value]": "",
            "columns[3][search][regex]": "false",
            "columns[4][data]": "4",
            "columns[4][name]": "forensic_id",
            "columns[4][searchable]": "true",
            "columns[4][orderable]": "false",
            "columns[4][search][value]": "",
            "columns[4][search][regex]": "false",
            "columns[5][data]": "5",
            "columns[5][name]": "approvaldate",
            "columns[5][searchable]": "true",
            "columns[5][orderable]": "true",
            "columns[5][search][value]": "",
            "columns[5][search][regex]": "false",
            "columns[6][data]": "6",
            "columns[6][name]": "atc",
            "columns[6][searchable]": "true",
            "columns[6][orderable]": "true",
            "columns[6][search][value]": "",
            "columns[6][search][regex]": "false",
            "columns[7][data]": "7",
            "columns[7][name]": "strength",
            "columns[7][searchable]": "true",
            "columns[7][orderable]": "true",
            "columns[7][search][value]": "",
            "columns[7][search][regex]": "false",
            "columns[8][data]": "8",
            "columns[8][name]": "dosageform",
            "columns[8][searchable]": "true",
            "columns[8][orderable]": "true",
            "columns[8][search][value]": "",
            "columns[8][search][regex]": "false",
            "columns[9][data]": "9",
            "columns[9][name]": "ingredientCount",
            "columns[9][searchable]": "true",
            "columns[9][orderable]": "true",
            "columns[9][search][value]": "",
            "columns[9][search][regex]": "false",
            "columns[10][data]": "10",
            "columns[10][name]": "status",
            "columns[10][searchable]": "true",
            "columns[10][orderable]": "true",
            "columns[10][search][value]": "Active",
            "columns[10][search][regex]": "false",
            "columns[11][data]": "11",
            "columns[11][name]": "drug_id",
            "columns[11][searchable]": "true",
            "columns[11][orderable]": "true",
            "columns[11][search][value]": "",
            "columns[11][search][regex]": "false",
            "columns[12][data]": "12",
            "columns[12][name]": "anatomy",
            "columns[12][searchable]": "true",
            "columns[12][orderable]": "true",
            "columns[12][search][value]": "",
            "columns[12][search][regex]": "false",
            "columns[13][data]": "13",
            "columns[13][name]": "therapeutic",
            "columns[13][searchable]": "true",
            "columns[13][orderable]": "true",
            "columns[13][search][value]": "",
            "columns[13][search][regex]": "false",
            "columns[14][data]": "14",
            "columns[14][name]": "pharmacology",
            "columns[14][searchable]": "true",
            "columns[14][orderable]": "true",
            "columns[14][search][value]": "",
            "columns[14][search][regex]": "false",
            "columns[15][data]": "15",
            "columns[15][name]": "chemical",
            "columns[15][searchable]": "true",
            "columns[15][orderable]": "true",
            "columns[15][search][value]": "",
            "columns[15][search][regex]": "false",
            "columns[16][data]": "16",
            "columns[16][name]": "roa",
            "columns[16][searchable]": "true",
            "columns[16][orderable]": "true",
            "columns[16][search][value]": "",
            "columns[16][search][regex]": "false",
            "columns[17][data]": "17",
            "columns[17][name]": "synonyms",
            "columns[17][searchable]": "true",
            "columns[17][orderable]": "true",
            "columns[17][search][value]": "",
            "columns[17][search][regex]": "false",
            "columns[18][data]": "18",
            "columns[18][name]": "reclassified_group",
            "columns[18][searchable]": "true",
            "columns[18][orderable]": "true",
            "columns[18][search][value]": "",
            "columns[18][search][regex]": "false",
            "order[0][column]": "0",
            "order[0][dir]": "desc",
            "start": "0",
            "length": "2000000000",
            "search[value]": "",
            "search[regex]": "false",
            "wdtNonce": "cd9a61cdc6",
            "sRangeSeparator": "|"}


class PharmFairSpider(Spider):
    name = "pharm_fair"
    start_urls = ['https://pharmfair.com/']
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) '
                      'Chrome/22.0.1207.1 Safari/537.1',
        'Connection': 'keep-alive',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Cache-Control': 'no-cache',
        'X-Requested-With': 'XMLHttpRequest'
    }

    def parse(self, response):
        return FormRequest('https://pharmfair.com/wp-admin/admin-ajax.php?action=get_wdtable&table_id=3',
                           method='POST', formdata=formdata, headers=self.headers,
                           callback=self.parse_dtls)

    def parse_dtls(self, response):
        data = json.loads(response.body)['data']
        fields = {'name': 1, 'ingredient': 2, 'license_holder': 3,
                  'regulatory': 4, 'dosage_form': 8, 'status': 10, 'roa': 16}
        print(len(data))
        for each in data:
            yield {key: each[value] for key, value in fields.items()}